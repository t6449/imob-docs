update inventory.property 
	set file_url = 'http://localhost:8090/api/v1/files/4cd4bb88-4139-4465-8562-83462a3f00ea'
;

-- Procedure that update 

CREATE PROCEDURE inventory.TEMP_updateUrlFileProperties()
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
	DECLARE productID bigserial DEFAULT NULL;

	-- declare cursor
	DEClARE curProduct
		CURSOR FOR
			SELECT product_id FROM inventory.property;

	-- declare NOT FOUND handler
	DECLARE CONTINUE HANDLER
        FOR NOT FOUND SET finished = 1;

	OPEN curProduct;

	readLoop: LOOP
		FETCH curProduct INTO productID;
		IF finished = 1 THEN
			LEAVE readLoop;
		END IF;
		UPDATE inventory.property p
        SET p.file_url = concat('http://localhost:8090/api/v1/',productID)
	    WHERE p.product_id = productID	;
	END LOOP;

	CLOSE curProduct;

END$$;

CALL inventory.TEMP_updateUrlFileProperties();